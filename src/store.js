import { applyMiddleware, createStore } from 'redux';
import { combineReducers } from 'redux';

import logger from 'redux-logger';
import thunk from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';

// reducer import to the store

import userReducer from './User/reducer.js';

const middleware = applyMiddleware(promiseMiddleware, thunk, logger);

export default createStore(combineReducers({
    userReducer

}), middleware)