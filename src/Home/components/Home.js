import React , {Component} from 'react';
import Loading from "../../Common/components/Loading.js";

import {connect} from "react-redux";
class Home extends Component {
    constructor(props) {
      console.log(props);
      
      super(props);
    
    }

    
    handleOnChange(e,key){
        // console.log(key);
        // console.log(e.target.value);
        
        // let user = {...this.props.userReducer.user}
        // user[key] = e.target.value
        
    
        // this.props.dispatch(setUser(user))
    }

    render(){
        
        return (
           
            
            <div>
                { this.props.userReducer.fetching && 
                 <Loading /> 
                }

                {!this.props.userReducer.fetching && 
                        <div className="stand">
                            <div className="outer-screen">
                                <div className="inner-screen">
                                    <div className="form">
                                 
                                    <input 
                                        type="text" 
                                        name="password" 
                                        placeholder="Password"
                                        value={this.props.userReducer.user.password ? this.props.userReducer.user.password : ''}
                                        onChange={val => this.handleOnChange( val,'password',)}
                                        />
                                    <input 
                                        type="submit" 
                                        defaultValue="Login" 
                                        />
                                    
                                    </div> 
                                </div> 
                            </div> 
                        </div>
                }

               
            </div>
            
          
          );
    }
 
}

export default connect(
  (store) => {
    return {
      userReducer: store.userReducer
    }
  }
)(Home);


