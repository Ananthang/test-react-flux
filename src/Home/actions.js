import userSource from './source';

export function fetchUser() {
    return function (dispatch) {
        dispatch({type: 'FETCH_USER', payload: userSource.fetchUser()});
    }
}

export function setUser(user) {
    return function (dispatch) {
        dispatch({type: 'SET_USER', payload:user});
    }
}
export function clearUser() {
    return function (dispatch) {
        dispatch({type: 'CLEAR_USER'});
    }
}

