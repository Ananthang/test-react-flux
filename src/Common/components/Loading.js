import React , {Component} from 'react';
import loadingAnim from "../static/giphy.gif";

import '../static/loading.css';
class Loading extends Component {
    
    render(){
        
        return (
            <div>
                <img src={loadingAnim} alt="giphy" className="center"></img>

            </div>
          
          );
    }
 
}

export default Loading;


