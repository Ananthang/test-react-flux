
import React from 'react';
import { Route,Redirect } from 'react-router-dom';
import Auth from "../../User/components/Auth.js";
function PrivateRoute({ component: Component,...rest }) {
  
  return(
    <Route
      {...rest}
      render={(props) => Auth.getAuth() === true
        ? <Component {...props} />
        : <Redirect to={{pathname: 'login', state: {from: props.location}}} />}
    />
  );
}

export default PrivateRoute;