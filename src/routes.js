import React from "react";
import {
  Router,
  Route
} from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store.js";
import App from './App.js';
import Home from "./Home/components/Home.js";
import { createBrowserHistory } from "history";
import LoginModel from "./User/components/LoginModel.js";
import RegisterModel from "./User/components/RegisterModel.js";
import PrivateRoute from "./Common/Auth/PrivateRoute.js";

const browserHistory = createBrowserHistory();
// let  user = JSON.parse(localStorage.getItem('user')) ? JSON.parse(localStorage.getItem('user')) : {}
// const isAuthed = user.id ? true : false; 
export default (
    <Provider store={store}>
    <Router history={browserHistory}>
            <Route exact path="/" component={App} />
            <Route path="/login" component={LoginModel} />
            <Route path="/register" component={RegisterModel} />
            <PrivateRoute  path="/home" component={Home} />

       
    </Router>
    </Provider>
);

