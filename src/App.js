import React , {Component} from 'react';
import './App.css';
import ViewUser from "./User/components/ViewUser.js"
import LoginModel from './User/components/LoginModel';



export default class App extends Component {
  render() {
    let user = JSON.parse(localStorage.getItem('user')) ? JSON.parse(localStorage.getItem('user')) : {} ;
    return (
        <div>
          {
            !user.id && this.props.history.push("login")          

          }
          {
            user.id && <div>
               {
                            React.Children.map(this.props.children,
                                (child) => React.cloneElement(child, this.props)
                            )
                }
          </div> && this.props.history.push("home")
          }
          
          {this.props.children}
        </div>
    );
  }
 
}
