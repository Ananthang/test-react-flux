import axios from 'axios';
const baseURL = 'https://jsonplaceholder.typicode.com/';


let getAccessToken = function () {
    try {
        return JSON.parse(localStorage.getItem('user')).accessToken
    } catch (e) {
        return "";
    }
}

const prepareConfig = axios.create({
    headers: {'Content-Type': 'application/json; charset=utf-8', Authorization: getAccessToken()}
});

let atRequest = {
    get:async function (uri) {
       return axios.get(baseURL + uri , prepareConfig)
            .then(function (response) {
                
               return response;
            })
            .catch(function (error) {
                return error;
                // dispatch(failureFunction(error))
            })
       
    }, 
    getById: async function (uri,body) {
        return axios.get(baseURL + uri , prepareConfig)
            .then(function (response) {
                // handle success
                return response;
                // console.log(response);
            })
            .catch(function (error) {
                // handle error
                return error;
                // console.log(error);
            })
            // .finally(function () {
            //     // always executed
            // })
    },
    post: async function (uri,body) {
        return axios.post(baseURL + uri , body , prepareConfig)
            .then(function (response) {
                // handle success
                return response;
                // console.log(response);
            })
            .catch(function (error) {
                // handle error
                return error;
                // console.log(error);
            })
            // .finally(function () {
            //     // always executed
            // })
    },
    patch: async function (uri,body) {
        return axios.patch(baseURL + uri , body , prepareConfig)
            .then(function (response) {
                // handle success
                return response;
                // console.log(response);
            })
            .catch(function (error) {
                // handle error
                return error;
                // console.log(error);
            })
            // .finally(function () {
            //     // always executed
            // })
    },
    put: async function (uri,body) {
        return axios.put(baseURL + uri , body , prepareConfig)
            .then(function (response) {
                // handle success
                return response;
                // console.log(response);
            })
            .catch(function (error) {
                // handle error
                return error;
                // console.log(error);
            })
            // .finally(function () {
            //     // always executed
            // })
    },
    delete: async function (uri,body) {
        return axios.delete(baseURL + uri , body , prepareConfig)
            .then(function (response) {
                // handle success
                return response;
                // console.log(response);
            })
            .catch(function (error) {
                // handle error
                return error;
                // console.log(error);
            })
            // .finally(function () {
            //     // always executed
            // })
    }
};

export default atRequest;