import React , {Component} from 'react';
import Loading from "../../Common/components/Loading.js";
import {
    Link, NavLink
} from "react-router-dom";
import "../static/login.css"
import {setUser, fetchUser, clearUser} from '../actions'; 

import {connect} from "react-redux";
import Auth from "./Auth.js";
class LoginModel extends Component {
    constructor(props) {
      
      super(props);
    
    }
    // handleOnChange(key , value,validation){
    handleOnChange(e,key){
        
        let user = {...this.props.userReducer.user}
        user[key] = e.target.value
        
    
        this.props.dispatch(setUser(user));
    }
    loginSubmit(){
        
        this.props.dispatch(fetchUser()) ;
    }
    componentDidMount(){
        let user = JSON.parse(localStorage.getItem('user')) ? JSON.parse(localStorage.getItem('user')) :{};
        if(user.id){
            Auth.authenticate();
            this.props.history.push("home");
            
        }
    
    }
    componentDidUpdate(){
        if(!this.props.userReducer.fetching && this.props.userReducer.fetched){
            localStorage.setItem('user',JSON.stringify(this.props.userReducer.user));
            Auth.authenticate();
            this.props.history.push("/")
            
        }
    }

    render(){
        
        return (
           
            
            <div>
                { this.props.userReducer.fetching && !this.props.userReducer.fetched &&
                 <Loading /> 
                }

                {!this.props.userReducer.fetching && !this.props.userReducer.fetched &&
                        <div className="stand">
                            <div className="outer-screen">
                                <div className="inner-screen">
                                    <input 
                                        type="text"
                                        name="username" 
                                        className="zocial-dribbble" 
                                        placeholder="Enter your email"
                                        value={this.props.userReducer.user.username ? this.props.userReducer.user.username : ''}
                                        onChange={val => this.handleOnChange( val,'username',)}
                                        />
                                    <input 
                                        type="text" 
                                        name="password" 
                                        placeholder="Password"
                                        value={this.props.userReducer.user.password ? this.props.userReducer.user.password : ''}
                                        onChange={val => this.handleOnChange( val,'password',)}
                                        />
                                    <button 
                                        className="at-btn"
                                        onClick={()=> this.loginSubmit()}  
                                        >Submit</button>
                                        <Link to="/register">Sign Up</Link>
                                    
                                </div> 
                            </div> 
                        </div>
                }

                {!this.props.userReducer.fetching && this.props.userReducer.fetchedError &&
                        <div className="stand">
                            <div className="outer-screen">
                                <div className="inner-screen">
                                    <input 
                                        type="text"
                                        name="username" 
                                        className="zocial-dribbble" 
                                        placeholder="Enter your email"
                                        value={this.props.userReducer.user.username ? this.props.userReducer.user.username : ''}
                                        onChange={val => this.handleOnChange( val,'username',)}
                                        />
                                    <input 
                                        type="text" 
                                        name="password" 
                                        placeholder="Password"
                                        value={this.props.userReducer.user.password ? this.props.userReducer.user.password : ''}
                                        onChange={val => this.handleOnChange( val,'password',)}
                                        />
                                    <button 
                                        className="at-btn"
                                        onClick={()=>this.loginSubmit('true')} 
                                        >Submit</button>
                                     <Link to="/register">Sign Up</Link>
                                    </div> 

                                    <div>Error</div>
                            </div> 
                        </div>
                }
                {/* { !this.props.userReducer.fetching && this.props.userReducer.fetched &&  localStorage.setItem('user',JSON.stringify(this.props.userReducer.user)) &&
                  this.props.history.push("home")
                } */}
               
            </div>
            
          
          );
    }
 
}

export default connect(
  (store) => {
    return {
      userReducer: store.userReducer
    }
  }
)(LoginModel);


